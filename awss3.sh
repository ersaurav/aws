#!/bin/sh
#!/bin/bash

# Usage info for the Script
if [ $# -lt 2 ]
then
	echo "Usage: $0 <AWS_ACCESS_KEY_ID> <AWS_SECRET_ACCESS_KEY>"
	echo "Example: $0 AKIAJK2FNZRFTGHY pEH/x8TyqlUiYrjIU9jKHnZwT7yff/FGTtfrd/"
	exit 1
fi

# Displays the details of the buckets in S3 with respect to account
echo 'Below are the buckets in the respective S3'
AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws s3 ls --region us-east-2
AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws s3 ls --region us-east-2 >a.txt
awk '{print substr($0,21)}' a.txt > b.txt

# Displays the details of the files in the respctive bucket
while read p; do
echo 'Below are the files as per S3 bucket' -$p
AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws s3 ls s3://$p --region us-east-2
done <b.txt
